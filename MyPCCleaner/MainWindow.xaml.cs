﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace MyPCCleaner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DirectoryInfo winTemp;
        public DirectoryInfo appTemp;
        public int total;
        public string version;

        public MainWindow()
        {
            InitializeComponent();
            winTemp = new DirectoryInfo(@"C:\Windows\Temp");
            appTemp = new DirectoryInfo(System.IO.Path.GetTempPath());
            total = 0;
            CheckActu();
            version = "1.0.0";
            LoadDate();
        }

        /// <summary>
        /// Vérification des actus sur le site web
        /// </summary>
        public void CheckActu()
        {
            string url = "http://localhost/udemy/01-csharp-wpf-siteweb/actu.json";
            using (WebClient client = new WebClient())
            {
                string jsonString = client.DownloadString(url);
                try
                {
                    JsonDocument json = JsonDocument.Parse(jsonString);
                    JsonElement news = json.RootElement.GetProperty("news");

                    Console.WriteLine("news.GetArrayLength(): {0}", news.GetArrayLength());

                    if (news.GetArrayLength() > 0)
                    {
                        JsonElement element = news[0];
                        string title = element.GetProperty("title").ToString();
                        string content = element.GetProperty("content").ToString();
                        actu.Content = title + " : " + content;
                        actu.Visibility = Visibility.Visible;
                        bandeauActu.Visibility = Visibility.Visible;
                        Console.WriteLine("news: {0} : {1}", title, content);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Une erreur est survenue pendant la lecture des actus : {0}", ex.Message);
                }
            }
        }

        /// <summary>
        /// Vérification de la dernière version disponible
        /// </summary>
        public void CheckVersion()
        {
            string url = "http://localhost/udemy/01-csharp-wpf-siteweb/version.json";
            using (WebClient client = new WebClient())
            {
                string jsonString = client.DownloadString(url);
                try
                {
                    JsonDocument json = JsonDocument.Parse(jsonString);
                    string version = json.RootElement.GetProperty("version").ToString();

                    if (this.version != version)
                    {
                        MessageBox.Show("Une mise à jour est disponible.", "Mise à jour", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Votre logiciel est à jour.", "Mise à jour", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Une erreur est survenue pendant la lecture de la version disponible : {0}", ex.Message);
                }
            }
        }

        /// <summary>
        /// Calcul de la taille d'un dossier
        /// </summary>
        /// <param name="directory">dossier dont on veut calculer la taille</param>
        /// <returns>taille totale des fichiers</returns>
        public long DirSize(DirectoryInfo directory)
        {
            return directory.GetFiles().Sum(file => file.Length) + directory.GetDirectories().Sum(subdirectory => DirSize(subdirectory));
        }

        /// <summary>
        /// Vider un dossier
        /// </summary>
        /// <param name="directory">Dossier à vider</param>
        public void ClearDirectory(DirectoryInfo directory)
        {
            foreach( FileInfo file in directory.GetFiles())
            {
                try
                {
                    file.Delete();
                    Console.WriteLine("Delete : {0}", file.FullName);
                    total++;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Protected file : {0}", file.FullName);
                    continue;
                }
            }

            foreach(DirectoryInfo subdirectory in directory.GetDirectories())
            {
                ClearDirectory(subdirectory);
                directory.Delete();
            }
        }

        /// <summary>
        /// Clique sur le bouton [Mise à jour]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_mise_a_jour_Click(object sender, RoutedEventArgs e)
        {
            CheckVersion();
        }

        /// <summary>
        /// Clique sur le bouton [Historique]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_historique_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("TODO: Créer page hisrtorique.", "Historique", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        /// <summary>
        /// Clique sur le bouton [Site Web]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_site_web_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(new ProcessStartInfo("http://www.anthony-cardinale.fr/pc-cleaner")
                {
                    UseShellExecute = true
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur : {0}", ex.Message);
            }
        }

        /// <summary>
        /// Clique sur le bouton [Analyser]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_analyser_Click(object sender, RoutedEventArgs e)
        {
            AnalyseFolders();
        }

        /// <summary>
        /// Analyse des dossiers
        /// </summary>
        public void AnalyseFolders()
        {
            Console.WriteLine("Début de l'analyse...");
            double totalSize = 0;

            try
            {
                totalSize += DirSize(winTemp) / 1024.0 / 1024.0; // convert bytes to MB
                totalSize += DirSize(appTemp) / 1024.0 / 1024.0; // convert bytes to MB

                string today = DateTime.Today.ToString("dd/MM/yyyy");

                espace.Content = Math.Round(totalSize, 0) + " Mo";
                date.Content = today;
                titre.Content = "Analyse effectuée !";
                SaveDate(today);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur à l'analyse des dossiers : {0}", ex.Message);
            }

            Console.WriteLine("Fin de l'analyse.");

        }

        /// <summary>
        /// Clique sur le bouton [Nettoyer]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_nettoyer_Click(object sender, RoutedEventArgs e)
        {
            CleanFolders();
        }

        /// <summary>
        /// Nettoyage des dossiers
        /// </summary>
        public void CleanFolders()
        {
            Console.WriteLine("Nettoyage en cours...");
            btn_nettoyer.Content = Environment.NewLine + Environment.NewLine + "NETTOYAGE EN COURS";

            Clipboard.Clear(); // nettoyage presse papier

            try
            {
                ClearDirectory(winTemp);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur pendant le nettoyage : {0}", ex.Message);
            }

            try
            {
                ClearDirectory(appTemp);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur pendant le nettoyage : {0}", ex.Message);
            }

            titre.Content = "Nettoyage effectué !";
            espace.Content = "0 Mo";

            Console.WriteLine("Nettoyage Terminé");
            btn_nettoyer.Content = Environment.NewLine + Environment.NewLine + "NETTOYAGE TERMINÉ";
        }

        /// <summary>
        /// Classe de formattage du fichier de persistance
        /// </summary>
        public class PersistenceFileFormat { public string LastAnalyse { get; set; } }

        /// <summary>
        /// Savegarde de la date de dernière analyse
        /// </summary>
        /// <param name="date"></param>
        public void SaveDate(string date)
        {
            PersistenceFileFormat persistence = new PersistenceFileFormat();
            persistence.LastAnalyse = date;

            string jsonString = JsonSerializer.Serialize(persistence);

            File.WriteAllText("persistance.json", jsonString);
        }

        public void LoadDate()
        {
            try
            {
                string jsonString = File.ReadAllText("persistance.json");
                PersistenceFileFormat persistence = JsonSerializer.Deserialize<PersistenceFileFormat>(jsonString);
                date.Content = persistence.LastAnalyse;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fichier de persistance inexistant.");
            }
        }
    }
}
